# 纯css实现的自画像
> 基本都是使用css的border、border-radius、transform属性将边框进行各种变形拼接而成

## 截图

未填充颜色前

![](https://gitee.com/qqoq/css-photo/raw/main/demo2.png)

填充颜色后

![](https://gitee.com/qqoq/css-photo/raw/main/demo1.png)
